# Display

This is a simple webite made with [pug](https://pugjs.org/) and [vue3](https://vuejs.org/) to display a list of slides in the form of a carousel.

## 1. Getting started

### 1.1. Setup

1. Clone this project with `git clone https://gitlab.cern.ch/mlhoutel/display.git`
2. Install [nodejs](https://nodejs.org/en/) (`v16+` recommended)
3. Setup with `npm install`

### 1.2. Run

```bash
npm run dev     # hot reload build with nodemon
npm run serve   # expose the build on http-server
npm run build   # build and reload the dist folder
```

### 1.3. Display script

If after having deployed vue-site-show at a given url, you want to schedule the display of the carousel website on a screen, you can use the `deploy/openFixedDisplay.sh` script:

 * Make sure you have Mozilla Firefox installed on the machine (version 71 onwards);
 * Define your custom url by setting a var `URL=''` with the path of the deployed carousel (default is `https://mro-dev.web.cern.ch/ci/mro/common/www/vue-site-show/`);
 * Finally, run the script to have the website displayed in full screen mode (ex: `URL=http://localhost:8080 ./deploy/openFixedDisplay.sh`)

Should you wish display it on a fixed screen, follow the [fixed-display-guide](https://mro-dev.web.cern.ch/docs/bcp/fixed-display-guide.html).

## 2. Edit

### 2.1. Displayed frames

You can change the order of the displayed frames, or even replace them with your own:

#### 2.1.1. Remote slides

1. Make sure that the resources to be displayed are `available via a url`, and that their headers do not contain the [X-Frame-Options](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options) property.
2. Place the urls to the assets to be displayed (web site, images, pdf...) in the `config.js` file as such

```js
SLIDES: [
    {
        URL: "{slide_url}.html", // the url to the slide asset
        DURATION: 5000,          // (optional) the slide duration in ms
        LOAD_ONCE: false         // (optional) load the slide only once (default: false)
    }
]
```

if the `DURATION` property is not specified for a given slide, the `BASE_SLIDE_DURATION` will ne applied from the `config.js` file:

```js
BASE_SLIDE_DURATION: 10000, // default slide duration in ms
```

The boolean property `LOAD_ONCE` comes in handy when a resource to be displayed requires long loading and rendering times.

#### 2.1.2. Local slides

1. Place the assets to be displayed (web site, images, pdf...) in the `static/carousel` folder.
2. Place the relative urls to the assets to be displayed in the `config.js` file as such:

```js
SLIDES: [
    {
        URL: "slides/{slide_source}.html", // the url to the slide asset
        DURATION: 20000,                   // (optional) the slide duration in ms
        LOAD_ONCE: false                   // (optional) load the slide only once (default: false)
    }
]
```

You can then run the `build.sh` script, that will compile the `index.pug` file and it's dependencies in `index.html`, and copy it and the static resources in the `dist` folder.

### 2.2. Delay and animations

You can also customise the duration and the style of the animations:

1. Edit the duration of the animation by changing the `:duration` property of the `transition-group` in the `src/style.pug` file (in ms)
2. Modify the `transition` duration in the `.list-enter-active` and `.list-leave-active` classes in `src/style.css`.

Should you wish to disable animations, you can add the `noAnimations=1` parameter to the URL query string. For instance:

```
https://mro-dev.web.cern.ch/ci/mro/common/www/vue-site-show/?noAnimations=1
```
