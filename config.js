const CONFIG = {
    RELOAD_INTERVAL: 15 * 60 * 1000, // page is not reloaded if there are no changes
    BASE_SLIDE_DURATION: 5000,
    SLIDES: [
        {
            URL: "static/slides/vistar.html",
            DURATION: 10000
        },
        {
            URL: "https://ab-atb-lpe.web.cern.ch/ab-atb-lpe/piquet/2.0/?fixedDisplay=true&subsystem=",
            DURATION: 15000
        },
        {
            URL: "https://cosmos.cern.ch/grafana/d/eaE7-jb7k/worldfip-segments-lhc?orgId=1&refresh=30s&kiosk&var-system=Cryogenic&var-system=Power&var-system=Quench&var-system=RF",
            DURATION: 10000,
            LOAD_ONCE: true
        }
    ],
};
