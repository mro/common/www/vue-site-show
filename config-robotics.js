
const CONFIG = {
    RELOAD_INTERVAL: 15 * 60 * 1000, // page is not reloaded if there are no changes
    BASE_SLIDE_DURATION: 5000,
    SLIDES: [
        {
            URL: "https://robotics.web.cern.ch/dashboards/tim?darkMode=1",
            DURATION: 10000
        },
        {
            URL: "https://wrap.cern.ch/dashboard/94360?_fixed_display_layout_override=1",
            DURATION: 20000
        },
        {
            URL: "https://ab-atb-lpe.web.cern.ch/ab-atb-lpe/piquet/2.0/?fixedDisplay=true&subsystem=",
            DURATION: 10000
        }
    ]
};
