/* global Vue, CONFIG */

// edit CONFIG from config.js

const RELOAD_INTERVAL = 15 * 60 * 1000; // Check every 15 mins if the page should be reloaded

var app = Vue.createApp({
  data() {
    return {
      slides: [],
      index: 0,
      timeoutHandler: null,
      intervalHandler: null,
      initialETag: undefined,
      initialDate: undefined,
      checkETag: true, // if false, fallback on check Date
      enableAnimations: true
    };
  },
  async mounted() {
    // Initialize slides from the config file
    this.slides = CONFIG.SLIDES.map((s) => ({
      url: s?.URL ?? "",
      duration: s?.DURATION || CONFIG.BASE_SLIDE_DURATION, // if no duration specified, we apply the base one
      loadOnce: s?.LOAD_ONCE === true
    }));

    // disable animation if required
    const urlParams = new URLSearchParams(window.location.search);
    const noAnimations = urlParams.get("noAnimations");
    this.enableAnimations = !(noAnimations === "true" || noAnimations === "1");

    // Initialize the recursive update loop
    this.loopNext();

    try {
      // Trigger reload check only if the file is hosted on a server
      if (!window.location.href.includes("http")) {
        console.warn("Website is not hosted on a server. Reload on source change disabled.");
        return;
      }

      // Check for supported reload method (ETage / Date)
      const response = await this.fetch(window.location.href, { method: 'HEAD' });

      // Empty => not supported (no comparison availlable)
      this.initialETag = response.getResponseHeader('ETag') ?? undefined;

      // Empty => not supported (will always return current date)
      this.initialDate = response.getResponseHeader('Last-Modified') ?? undefined;

      // If no server method available, cancel the reloading check
      if (this.initialETag === undefined && this.initialDate === undefined) {
        console.warn("Neither 'ETag' or 'Last-Modified' methods are supported by the server. Reload on source change disabled.");
        return;
      }
      // If Etag is not supported by the server, fallback on Date check
      else if (this.initialETag === undefined) {
        console.info("'ETag not supported by the server, fallback on Date check");
        this.checkETag = false;
      }

      // Initialize the source reload loop
      this.intervalHandler = setInterval(async () => { await this.checkSource(); }, CONFIG.RELOAD_INTERVAL ?? RELOAD_INTERVAL);
    }
    catch (err) {
      console.error(err);
    }
  },
  beforeDestroy() {
    clearTimeout(this.timeoutHandler);
    clearInterval(this.intervalHandler);
  },
  methods: {
    isCurrent(index) {
      return this.index === index;
    },
    isPreRenderedElement(index) {
      return (this.index + 1) % this.slides.length === index;
    },
    isRendered(index) {
      return this.isCurrent(index) || this.isPreRenderedElement(index) ||
        this.slides[index].loadOnce;
    },
    isVisible(index) {
      return this.isCurrent(index);
    },
    goNext() {
      this.index = (this.index + 1) % this.slides.length;
    },
    loopNext() {
      this.timeoutHandler = setTimeout(() => {
        this.goNext(); // Go to the next slide pannel
        this.loopNext(); // Recursive call
      }, this.slides[this.index].duration); // Inject the current slide duration
    },
    /* Custom fetch to prevent the ETag issue on firefox */
    fetch(url, config) {
      return new Promise(function(resolve, reject) {
        const xhr = new XMLHttpRequest();
        xhr.open(config.method, url);

        if (config.headers) {
          for (const [name, value] of Object.entries(config.headers)) {
            xhr.setRequestHeader(name, value);
          }
        }
        xhr.onload = () => resolve(xhr);
        xhr.onerror = (error) => reject(error);

        xhr.send();
      });
    },
    async checkSource() {
      const STATUS_OK = 200;

      const headers = {};

      if (this.checkETag) {
        // Compare with the ETag fetched at page load
        headers['If-None-Match'] = this.initialETag;
      }
      else {
        // Compare with last modified date stored at the beginning
        headers['If-Modified-Since'] = this.initialDate;
      }

      // Retrieve information from the sources of the page to see if it should be reloaded
      const response = await this.fetch(window.location.href, { method: 'HEAD', headers });

      // Check if the document was modified since load
      if (response.status === STATUS_OK) {

        // To avoid inconsistencies between browsers (return OK but different ETag)
        if (this.checkETag && this.initialETag === response.getResponseHeader('ETag')) {
          return;
        }

        window.location.reload(); // We refresh the page
      }
    }
  }
});

app.mount("#app");
